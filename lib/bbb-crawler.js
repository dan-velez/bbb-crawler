/** bbb-crawler.js - crawl bbb.org. ***************************/

const rendfetch = require('rendfetch');
const { EventEmitter } = require('events');
const cheerio = require('cheerio');

module.exports = bbb_crawler;

const root = "https://www.bbb.org";
let emitter = new EventEmitter();
let queue = [];
let visited = {};

/** bbb_crawler ***********************************************/

async function bbb_crawler({
  loc,
  term,
  country,
  page
}) 
{
  /* Scrape bbb.org for business leads,
   * emitting a 'data' event for each lead found.
   * Returns an event emitter. */
  let url = cons_url(loc, term, page, country);

  /***DEBUG***/
 console.log("\n\n\nbbb-crawler @ [" + url + "]");

  rendfetch(url).then(html => {
    /* Get business links on current page. */
    let links = getlinks(html);

    /**DEBUG
    console.log('links pre filter:', links); 
    ***/

    links = links.filter(l => {
      return l.includes("bbb.org") && l.includes("business-reviews");
    });

    /***DEBUG***/
    if(html.length <= 1024) {
      console.log("html:", html.length, "bytes");
      console.log(html); 
    }
    else {
      console.log("html:", Math.round(html.length/1024), "kB");
      // console.log(html);
    }
    console.log("Found", links.length, "links on page");
    console.log(links);

    /* Use async library here on each link... */
    concat_infs(links, ()=> {
      /* Recursive call after all businesses on this page are scraped. */
      let $ = cheerio.load(html);
      let next_link = $("a[title='Next']");
      if(next_link.text()) {
        bbb_crawler({
          loc,
          term,
          country,
          page: page+1
        });
      } else {
        emitter.emit("end");
        return;
      }
    });

  }); /* End `rendfetch` call. */

  return emitter;
}

/** getlinks **************************************************/

function getlinks(html) {
  let results = [];
  let $ = cheerio.load(html);
  let as = $('body').find('a');
  as.each(function(i, e) {
    let href = $(this).attr('href');
    /* filter unwanted links. */
    if(href != undefined &&
      (href.startsWith("/") || href.startsWith("http")) &&
      results.indexOf(href) < 0) {
      /* Fix broken links. */
      if(href.startsWith("//")) href = "http:"+href;
      results.push(href);
    }
  });
  return results;
}

/** cons_url **************************************************/

function cons_url(loc, term, page, country) {
  let prefix = "";
  if(country == "USA") prefix = "/en/us/";
  else if(country == "MEX") prefix = "/es/mx/";
  else if(country == "CAN") prefix = "/en/ca/"
  return `${root}${prefix}search?` + 
    `inputText=${encodeURIComponent(term)}` + 
    `&locationText=${encodeURIComponent(loc)}` + 
    `&locationLatLng=&page=${page}`;
}

/** parse_page ************************************************/

function get_biz_info(link, callb) {
  /* Parse the html page at `link` for bbb.org leads. */
  if(!link) return callb({});
  rendfetch(link).then(html => {
    let $ = cheerio.load(html);
    let address = $('address').text();
    let emails =  html.match(/[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}/ig);
    let email = "";
    if(emails) emails = emails.filter((e)=> !e.match(/bbb/ig));
    if(emails && emails.length) email = emails[0]; else email = "";
    if(address) address = address.split(/\s{2,}/g).join(' ').trim();
    let res = {
      name: $('div>h1.address__sub-heading').text(),
      email: email,
      phone: $('a.address__phone-number-link').text(),
      address: address
    };
    emitter.emit('data', res);
    callb(res);
  });
}

/** concat_inf ************************************************/

async function concat_infs(links, callb) {
  /* Parse each page from `links` array. */
  if(links.length <= 0) {
    return callb();
  } else {
    /* Search next link in links queue. */
    let link = links.shift();
    get_biz_info(link, inf => {
      concat_infs(links, callb);
    });
  }
}
