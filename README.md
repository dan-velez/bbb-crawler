# bbb-crawler
## Description
A command line tool that outputs leads to a `.csv` file

## Installation
Run the following command in your terminal:

```
# mac
brew update
brew install node

# bbb-crawler command line program
npm i -g bbb-crawler
```

## Usage
```
bbb-crawler [-t term] [-l location] [-o output file]
-term       A word or sentence describing what you are looking for

-output     Path to a `.csv` file to save the output to.
            Can be relative or absolute

-location   City 2-letter state code, and country in US, Canada, or Mexico
            (e.g. New York, NY USA | Vancouver, BC CAN | Tijuana, BCN MEX)
```

## Examples
```
bbb-crawler -t Driving Lessons -l Encino, California USA -o driving-lesson-leads-ENCA.csv
bbb-crawler -t Pizza parlor -l Tijuana, BCN MEX -o my-leads.csv
bbb-crawler -t Auto Body Shop -l Vancouver, BC CAN -o my-leads.csv
```

## Troubleshooting
#### Running multiple searches
You can open several terminals and run simultaneous searches. They cannot, however, be writing to the same file. They will eventually both open the file at the same time and data will be lost.
